//
//  ViewController.swift
//  ForcedAppUpgrade
//
//  Created by Samuel Rebeles (CE CEN) on 6/10/15.
//  Copyright (c) 2015 Samuel Rebeles (CE CEN). All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class ViewController: UIViewController {
    
    //This is a stub variable to pretend I discovered the current app version easily
    let currentWFMVersion = "5.2.0"
    let currentPlatform = "ios"
    
    var actvityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
    let data = NSMutableData()
    var appVersions = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.actvityIndicator.center = self.view.center
        self.actvityIndicator.hidesWhenStopped = true
        self.actvityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(self.actvityIndicator)
        self.actvityIndicator.startAnimating()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        //let paramString:String = "'where={\"version\":\"\(currentWFMVersion)\"}'"
        let baseUrl = NSURL(string:"https://api.parse.com/1/classes/AppVersion")
        let request = NSMutableURLRequest(URL: baseUrl!)
    
        var error: NSError?
        let getParameters = ["version":"\(currentWFMVersion)"]
        var paramsJSON: NSData?
        
        paramsJSON = NSJSONSerialization.dataWithJSONObject(getParameters, options: nil, error: &error)
        
        let paramsJSONString = NSString(data: paramsJSON!, encoding: NSUTF8StringEncoding)!
        let whereClause = paramsJSONString.stringByAddingPercentEscapesUsingEncoding(NSASCIIStringEncoding)
        
        request.HTTPMethod = "GET"
        request.setValue("1b7NfDFtIxtGUGLssXMbQzotNlK8ltKH0soUDnFw", forHTTPHeaderField: "X-Parse-Application-Id")
        request.setValue("FcfwXtsolpipnOVR9YPjMVIkYUijXRy1l8f9AMJW", forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let requestURL = NSURL(string: String(format: "%@?%@%@", baseUrl!, "where=", whereClause!))
        request.URL = requestURL!

        let urlConnection = NSURLConnection(request: request, delegate: self)
        urlConnection?.start()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        
        let json:JSON = JSON(data:data)
        
        if let versionResults: Array = json["results"].array {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("UpgradeViewController") as! UpgradeViewController
            
            //vc.urlString = versionResults[0]["app_store_url"].string!
            vc.view.frame = self.view.frame
            
            //Parse what I get from parse into the modal
            vc.upgradeMessageText.text = versionResults[0]["message"].string
            
            //Set the app store url
            vc.appStoreButton.setTitle(versionResults[0]["app_store_url"].string, forState: UIControlState.Normal)
            vc.appStoreButton.titleLabel?.hidden = true
            
            //Check to see if we allow them to continue to use the application with its current version
            if versionResults[0]["forced"] == "false" {
                vc.upgradeMessageTitle.text = "RECOMMENDED UPDATE"
            }
            //Present the modal view controller
            vc.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            self.navigationController?.presentViewController(vc, animated: true, completion: nil)
        }
        
        self.actvityIndicator.stopAnimating()
    }
}


class UpgradeViewController: UIViewController {
    
    @IBOutlet weak var appStoreButton: UIButton!
    @IBOutlet weak var upgradeModalView: UIView!
    @IBOutlet weak var upgradeMessageTitle: UILabel!
    @IBOutlet weak var upgradeMessageText: UILabel!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.upgradeModalView.layer.cornerRadius = 3.0
    }
    @IBAction func laterAction(sender: AnyObject) {
        self.view.removeFromSuperview()
    }
    @IBAction func appUpgradeSuccess(sender: UIButton) {
        let escapedURLString = self.appStoreButton.titleLabel?.text
        print(escapedURLString)
        let appStoreUrl = NSURL(string: escapedURLString!)
        UIApplication.sharedApplication().openURL(appStoreUrl!)
    }
}