//
//  AppDelegate.swift
//  ForcedAppUpgrade
//
//  Created by Samuel Rebeles (CE CEN) on 6/10/15.
//  Copyright (c) 2015 Samuel Rebeles (CE CEN). All rights reserved.
//

import UIKit

#if DEBUG
    import AdSupport
#endif

import Leanplum


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.None)
        
        
        #if DEBUG
            Leanplum.setDeviceId(ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString)
            Leanplum.setAppId("app_om2dsr1N5Ox3bDDVDruCJ841iU72kozOecqT6eNv2eU", withDevelopmentKey:"dev_BoN4ag3ZiCe1HOb7zJvbsVjHWghAkZMxfnsT5m3Ojyc")
            #else
            Leanplum.setAppId("app_om2dsr1N5Ox3bDDVDruCJ841iU72kozOecqT6eNv2eU", withProductionKey: "prod_zBSmsEPwphAlMhKsoQRJYj4XZrxC0ULUFfaGTN4PCZA")
        #endif
        
        
        // Syncs all the files between your main bundle and Leanplum.
        // This allows you to swap out and A/B test any resource file
        // in your project in realtime.
        Leanplum.syncResourcesAsync(true)
        
        // Tracks in-app purchases automatically as the "Purchase" event.
        // To require valid receipts upon purchase or change your reported
        // currency code from USD, update your app settings.
        Leanplum.trackInAppPurchases()
        
        // Tracks all screens in your app as states in Leanplum.
        Leanplum.trackAllAppScreens()
        
        // Starts a new session and updates the app content from Leanplum.
        Leanplum.start()
        
        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

